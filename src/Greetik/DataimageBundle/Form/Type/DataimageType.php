<?php
    namespace Greetik\DataimageBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\Extension\Core\Type\HiddenType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Form\Extension\Core\Type\UrlType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataimageType
 *
 * @author Paco
 */
class DataimageType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        
        $builder->add('title')
                    ->add('comments')
                    ->add('link', UrlType::class )
                    ->add('id', HiddenType::class, array("mapped" => false));

        if (count($options['_items'])>0){
            $builder->add('item_id', 'choice', array('required'=>false, 'empty_data'=>null, 'choices'=>$options['_items']));
        }
                    
    }
    
    public function getName(){
        return 'Dataimage';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\DataimageBundle\Entity\Dataimage',
            '_items' => array()
        ));
    }      
}

