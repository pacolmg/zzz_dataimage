<?php

namespace Greetik\DataimageBundle\Service;
use Greetik\DataimageBundle\Entity\Dataimage;
use Symfony\Component\HttpFoundation\Response;
use UploadHandler;
use finfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */

class Tools {
    
    private $em;
    private $rootdir;
    private $router;
    private $interfacetools;
    
    public function __construct($_entityManager, $_rootdir, $_router, $_interfacetools)
    {
        $this->em = $_entityManager;
        $this->rootdir = $_rootdir;
        $this->router = $_router;
        $this->interfacetools = $_interfacetools;
    }

    public function importImage($url, $item_id, $item_type, $filename='', $filetype='image'){
        if (!@getimagesize($url)) throw new \Exception('No se encontró la imagen a importar: '.$url.' -> '.@getimagesize($url));
        
        if (empty($filename)) $filename = $this->interfacetools->generateRandomUsername();
        
        $ext = explode('.', $url);
        $ext = $ext[count($ext)-1];
        $todest = $this->rootdir . '/../web/uploads/'.$item_type;
        if (!file_exists($todest)) mkdir($todest);
        $todest .= '/'.$item_id;
        if (!file_exists($todest)) mkdir($todest);
        
            
        $todest .= '/'.trim(str_replace(' ', '_',$filename)).'.'.$ext;
        
        if (file_exists($todest)) return '';
        
        
        @copy($url, $todest);
        
        $image = $this->createimage($filename, floatval(filesize($todest)), $item_type, $item_id, trim(str_replace(' ', '_',$filename)).'.'.$ext, $filetype);
    }
    
    /**
    * Upload an image
    * 
    * @param int $item_id The image is associated to an item with this id
    * @param string $item_type It's the type of the item that the image is associated to
    * @return the response of the uploadHandler.php
    * @author Pacolmg
    */
    public function uploadImage($item_id, $item_type, $filetype='image'){
        require_once($this->rootdir . '/../web/bundles/dataimage/plugins/jquery-file-upload/server/php/UploadHandler.php');
        
        //upload and save the image in db

        $upload_handler = new UploadHandler(
            array(
            'upload_dir'=>$this->rootdir . '/../web/uploads/'.$item_type.'/'.$item_id.'/',
            'upload_url'=>$this->rootdir . '/../web/uploads/'.$item_type.'/'.$item_id.'/'
            )
        );

        //get the title from the filename
        $filename = $upload_handler->response['files'][0]->name;
        $aux = explode('.', $filename); unset($aux[count($aux)-1]);

        $image = $this->createimage(str_replace('-', ' ',implode(' ', $aux)), floatval(($upload_handler->response['files'][0]->size)/1000), $item_type, $item_id, $filename, $filetype);
        
        //set the delete url and other params to show
         $upload_handler->response['files'][0]->deleteUrl =  $this->router->generate('dataimage_dropimage', array('id'=>$image->getId() ));
         $upload_handler->response['files'][0]->editUrl = $this->router->generate('dataimage_formimage', array('id'=>$image->getId()));
         $upload_handler->response['files'][0]->moveUrl = $this->router->generate('dataimage_formimagemove', array('id'=>$image->getId()));
         $upload_handler->response['files'][0]->name = $image->getTitle();
         $upload_handler->response['files'][0]->filetype = $image->getFiletype();
         $upload_handler->response['files'][0]->size = $image->getFilesize();

         list($upload_handler->response['files'][0]->url, $upload_handler->response['files'][0]->thumbnailUrl) = $this->getDataimagePath($item_type, $item_id, $image->getFilename());

         return $upload_handler->response;
    }
    
    protected function createimage($title, $size, $item_type, $item_id, $filename, $filetype){
        $image = new Dataimage();
        
        $image->setTitle($title);
        $image->setFilesize($size);
        
        $finfo = new \finfo();
        $image->setMimetype($finfo->file ($this->rootdir . '/../web/uploads/'.$item_type.'/'.$item_id.'/'.$filename));
        
        $image->setFiletype($filetype);
        
        $image->setComments('');
        $image->setNumorder($this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($item_type, $item_id, $filetype)+1);
        $image->setFilename($filename);
        $image->setItemid($item_id);
        $image->setItemtype($item_type);

        $this->em->persist($image);
        $this->em->flush();
        
        return $image;
    }    
    
    
    /**
     * Change the physical file from one item to another
     * 
     */
    public function changeDataimageitem($image, $olditemid){
        if (!file_exists($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/')) mkdir ($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/');
        rename($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$olditemid.'/'.$image->getFilename(), $this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/'.$image->getFilename());
        if (!file_exists($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/thumbnail/')) mkdir ($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/thumbnail/');
        @rename($this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$olditemid.'/thumbnail/'.$image->getFilename(), $this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/thumbnail/'.$image->getFilename());
        $this->em->flush();
        $this->reorderImages($olditemid, $image->getItemtype(), $image->getFiletype());        
    }
    
    /**
     * Get an image
     * 
     * @param int $id
     * @return Dataimage
     */
    public function getImage($id){
        return $this->em->getRepository('DataimageBundle:Dataimage')->findOneById($id);
    }
    
    /**
    * Get the images
    * 
    * @param int $item_id The images are associated to an item with this id
    * @param string $item_type It's the type of the item that the images are associated to
    * @return an array with the images
    * @author Pacolmg
    */    
    public function getImages($item_id, $item_type, $array_mode=1, $filetype='image', $strict=false){
             $data=array();
               
            $files = $this->em->getRepository('DataimageBundle:Dataimage')->findByItem($item_id, $item_type, $filetype, $strict);
            if (!$array_mode) return $files;
            
              foreach($files as $k=>$file){
                  list($data[$k]['url'], $data[$k]['thumbnailUrl']) = $this->getDataimagePath($file->getItemtype(), $file->getItemid(), $file->getFilename());
                  $data[$k]['id'] = $file->getId();
                  $data[$k]['filetype'] = $filetype;
                  $data[$k]['name'] = $file->getTitle();
                  $data[$k]['comments'] = $file->getComments();
                  $data[$k]['link'] = $file->getLink();
                  $data[$k]['deleteUrl']=$this->router->generate('dataimage_dropimage', array('id'=>$file->getId()));
                  $data[$k]['editUrl']=$this->router->generate('dataimage_formimage', array('id'=>$file->getId()));
                  $data[$k]['moveUrl']=$this->router->generate('dataimage_formimagemove', array('id'=>$file->getId()));
                  $data[$k]['size'] = $file->getFilesize();
                  $data[$k]['item_id'] = $file->getItemid();
                  $data[$k]['item_type'] = $file->getItemtype();
                  $data[$k]['deleteType'] = 'DELETE';
              }
             
             
            return array('errorCode'=>0, 'files'=>$data);
    }

    public function getDocuments($item_id, $item_type, $array_mode=1){
        return $this->getImages($item_id, $item_type, $array_mode, 'document');
    }
    
    /**
    * Drop an Image
    * 
    * @param int $id The id of the image to delete
    * @return the response of the uploadHandler.php
    * @author Pacolmg
    */    
    public function dropImage($id){
        error_reporting(E_ALL | E_STRICT);
        require_once($this->rootdir . '/../web/bundles/dataimage/plugins/jquery-file-upload/server/php/UploadHandler.php');
      
        $image = $this->em->getRepository('DataimageBundle:Dataimage')->findOneById($id);
        if (!$image) return array("error"=>"No se encontró la imagen");
        
        $_GET['file']=$image->getFilename();
        $upload_handler = new UploadHandler(
            array(
                'upload_dir'=>$this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/',
                'upload_url'=>$this->rootdir . '/../web/uploads/'.$image->getItemtype().'/'.$image->getItemid().'/',
                '_method'=>'DELETE'
            )
        );
        
        if (!isset($upload_handler->response['error'])){
            $this->em->remove($image);
            $this->em->flush();
        }else return $upload_handler->response;
        
        //reorder the images of the item
        return $this->reorderImages($image->getItemid(), $image->getItemtype(), $image->getFiletype());        
    }
    
    /**
    * Get the path of a image
    * 
    * @param int $id It's the id of the item that the image is associated to
    * @param string $type It's the type of the item that the image is associated to
    * @param string $filename It's the name that the image is saved in disk
    * @return array[0] the url of the image, and array[1] the url of the thumbnail of the image 
    * @author Pacolmg
    */
    public function getDataimagePath($type, $id, $filename, $withroot=0){
        return array((($withroot)?$this->rootdir.'/../web':'').'/uploads/'.$type.'/'.$id.'/'.$filename, (($withroot)?$this->rootdir.'/../web':'').'/uploads/'.$type.'/'.$id.'/thumbnail/'.$filename);
    }
    
    /**
    * Order of the images of an item, from 1 to n
    * 
    * @param int $item_id The images are associated to an item with this id
    * @param string $item_type It's the type of the item that the images are associated to
    * @return true/false
    * @author Pacolmg
    */    
    public function reorderImages($item_id, $item_type, $filetype){
        return $this->interfacetools->reorderItemElems($this->getImages($item_id, $item_type, 0, $filetype));
    }
  
    /**
    * Move an image from its old position to a new position
    * 
    * @param int $item_id The image is associated to an item with this id
    * @param string $item_type It's the type of the item that the image is associated to
    * @return the response of the uploadHandler.php
    * @author Pacolmg
    */    
    public function moveImage($id, $newposition, $filetype=''){
        $image = $this->getImage($id);
        if (empty($filetype)) $filetype = $image->getFiletype ();
        return $this->interfacetools->moveItemElem('DataimageBundle:Dataimage', $id, $newposition, '', $this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($image->getItemtype(), $image->getItemid(), $filetype, true), '', '', $filetype);
    }
    
    
    public function findTotalElems($filetype=''){
        return $this->em->getRepository('DataimageBundle:Dataimage')->findTotalElems($filetype);
    }
    
    public function findMaxNumOrderByElem ($type, $id_elem, $filetype){
        return $this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($type, $id_elem, $filetype);
    }
    
    public function modifyImage(){
        $this->em->flush();
    }    
}
