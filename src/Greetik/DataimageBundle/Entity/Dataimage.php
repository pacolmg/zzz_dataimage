<?php

namespace Greetik\DataimageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dataimage
 *
 * @ORM\Entity(repositoryClass="Greetik\DataimageBundle\Entity\DataimageRepository")
 * @ORM\Table(name="dataimage", indexes={
 *      @ORM\Index(name="item_type", columns={"item_type"}),  @ORM\Index(name="item_id", columns={"item_id"}),  @ORM\Index(name="filetype", columns={"filetype"}),  @ORM\Index(name="numorder", columns={"numorder"}),  @ORM\Index(name="item", columns={"item_id", "item_type"}),  @ORM\Index(name="itemorder", columns={"item_id", "item_type", "filetype", "numorder"})
 * })
 */
class Dataimage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text")
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="mimetype", type="string", length=255, nullable=true)
     */
    private $mimetype;
    
    /**
     * @var string
     *
     * @ORM\Column(name="filetype", type="string", length=255, nullable=true)
     */
    private $filetype;
    
    
    /**
     * @Assert\NotBlank()
     * @var float
     *
     * @ORM\Column(name="filesize", type="float")
     */
    private $filesize;

    /**
     * @Assert\NotBlank()
     * @var integer
     *
     * @ORM\Column(name="numorder", type="integer")
     */
    private $numorder;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @Assert\NotBlank()
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=true)
     */
    private $itemid;
  
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=255)
     */
    private $itemtype;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Dataimage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Dataimage
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     * @return Dataimage
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer 
     */
    public function getNumorder()
    {
        return $this->numorder;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Dataimage
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

   
    /**
     * Set filesize
     *
     * @param float $filesize
     * @return Dataimage
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return float 
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

  

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Dataimage
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Dataimage
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Dataimage
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     * @return Dataimage
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string 
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set filetype
     *
     * @param string $filetype
     * @return Dataimage
     */
    public function setFiletype($filetype)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype
     *
     * @return string 
     */
    public function getFiletype()
    {
        return $this->filetype;
    }
}
